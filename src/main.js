import Vue from 'vue'
import App from './App.vue'
import routes from "@/routes";
import VueRouter from "vue-router";
import VueMaterial from 'vue-material'

Vue.config.productionTip = false
Vue.use(VueMaterial)
Vue.use(VueRouter)


import { MdButton, MdContent, MdTabs } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdTabs)

export const EventBus = new Vue();
export const url = 'http://localhost:8080/'
const linkActiveClass = 'my-link-active-class';

// pass custom class to Vue Material
Vue.material.router.linkActiveClass = linkActiveClass;

const router = new VueRouter({
  routes: routes,
  mode: 'history',
  linkActiveClass
})

new Vue({
  render: h => h(App),
  router: router,
}).$mount('#app')
