import Login from "@/components/Login";
import Home from "@/components/Home";
import Clients from "@/components/tabs/Clients";
import Contacts from "@/components/tabs/Contacts";
import Employees from "@/components/tabs/Employees";
import Projects from "@/components/tabs/Projects";
import Researches from "@/components/tabs/Researches";
import Candidates from "@/components/tabs/Candidates";
import Users from "@/components/tabs/Users";

export default [
    {path: '/', component: Login},
    {path: '/home', component: Home,
     children :[
         {path: 'clients', name: 'clients', component: Clients},
         {path: 'contacts', name: 'contacts', component: Contacts},
         {path: 'employees', name: 'employees', component: Employees},
         {path: 'projects', name: 'projects', component: Projects},
         {path: 'researches', name: 'researches', component: Researches},
         {path: 'candidates', name: 'candidates', component: Candidates},
         {path: 'users', name: 'users', component: Users},

     ]}
]